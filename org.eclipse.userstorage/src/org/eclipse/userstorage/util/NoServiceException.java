/*
 * Copyright (c) 2015 Eike Stepper (Berlin, Germany) and others.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Eike Stepper - initial API and implementation
 */
package org.eclipse.userstorage.util;

import org.eclipse.userstorage.IStorage;

import java.io.Serializable;

/**
 * Signals that a {@link IStorage storage} is used, but no {@link IStorage#getService() service} could be assigned.
 *
 * @author Eike Stepper
 * @since 1.0
 */
public class NoServiceException extends IllegalStateException
{
  private static final long serialVersionUID = 1L;

  /**
   * Public constructor to make this exception {@link Serializable}.
   */
  public NoServiceException()
  {
  }
}
