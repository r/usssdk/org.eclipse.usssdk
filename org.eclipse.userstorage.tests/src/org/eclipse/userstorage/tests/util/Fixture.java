/*
 * Copyright (c) 2015 Eike Stepper (Berlin, Germany) and others.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Eike Stepper - initial API and implementation
 */
package org.eclipse.userstorage.tests.util;

import java.io.File;

/**
 * @author Eike Stepper
 */
public class Fixture
{
  public static final boolean REMOTE = Boolean.getBoolean("org.eclipse.userstorage.tests.remote");

  public static final File TEST_FOLDER = new File(System.getProperty("java.io.tmpdir"), "uss-tests");

  public Fixture()
  {
  }

  public void dispose() throws Exception
  {
  }
}
